import json
import uuid
from locust import HttpUser, TaskSet, task, SequentialTaskSet


class CreateAndGetCourse(SequentialTaskSet):
    course_ids = []

    @task
    def create_course(self):
        # response = self.client.post(
        #     url="/api/course",
        #     data=json.dumps({
        #         "name": "aws course #" + str(uuid.uuid4()),
        #         "description": "good random course",
        #         "platform": "youtube",
        #         "link": "http://www.youtube.com"
        #     }),
        #     headers={'content-type': 'application/json'})
        # id = response.text
        id = str(uuid.uuid4())
        self.client.get(url="/search?q=" + id, name="crear post")
        self.course_ids.append(id)

    @task
    def get_courses(self):
        print(self.course_ids)
        if self.course_ids:
            course_id = self.course_ids.pop(0)
            self.client.get(url="/search?q=" + course_id, name="get post by id")
            # self.client.get(
            #     url="/api/course/" + course_id,
            #     name="/api/course/[id]")


class GetCourses(TaskSet):
    @task
    def get_courses(self):
        #self.client.get(url="/api/course")
        self.client.get(url="/", name="get main post list")


class UserTaskSet(TaskSet):
    tasks = {CreateAndGetCourse: 1, GetCourses: 25}  # 25x times more get courses than create and get


class WebsiteUser(HttpUser):
    tasks = [UserTaskSet]
    min_wait = 1000  # cada usuario espera entre 1 y 2 segundos antes de hacer un nuevo request
    max_wait = 2000